import React from "react";
import Login from "./components/Login";
import Signup from "./components/Signup";
import { createStackNavigator } from "react-navigation";
const RootStack = createStackNavigator(
  {
    LogIn: { screen: Login },
    SignUp: { screen: Signup }
  },
  {
    initialRouteName: "LogIn"
  }
);
export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}
