import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ToastAndroid
} from "react-native";
const screenWidth = Math.round(Dimensions.get("window").width);
const screenHeight = Math.round(Dimensions.get("window").height);

export default class App extends React.Component {
  static navigationOptions = {
    title: "SignUp"
  };
  state = {
    email: "",
    password: "",
    confPassword: ""
  };
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.logo}>Financial Tracker</Text>
        <View style={styles.inputView}>
          <TextInput
            style={styles.inputText}
            placeholder="Email..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ email: text })}
          />
        </View>

        <View style={styles.inputView}>
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="Password..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ password: text })}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="confirm Password..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ confPassword: text })}
          />
        </View>
        <TouchableOpacity>
          <Text style={styles.forgot}>Forgot Password?</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.loginBtn}>
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={styles.loginText}>Signup</Text>
        </TouchableOpacity>

        <View style={styles.footerContainer}>
          <Text style={styles.footer}>Created by Hackour</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center"
  },
  logo: {
    fontWeight: "bold",
    fontSize: 30,
    color: "#0F6AA7",
    marginBottom: 40
  },
  inputView: {
    width: "80%",
    backgroundColor: "#F0F2F5",
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    padding: 20
  },
  inputText: {
    height: 50,
    color: "black"
  },
  forgot: {
    color: "#0F6AA7",
    fontSize: 11
  },
  loginBtn: {
    width: "80%",
    backgroundColor: "#0F6AA7",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10
  },
  loginText: {
    color: "white"
  },
  signUp: {
    color: "#0F6AA7"
  },
  footerContainer: {
    position: "absolute",
    color: "white",
    bottom: 0,
    backgroundColor: "white",
    width: screenWidth,
    alignItems: "center"
  },
  footer: {
    color: "#0F6AA7"
  }
});
